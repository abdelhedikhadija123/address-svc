FROM openjdk:8-jdk-alpine
#ARG JAR_FILE=target/*.jar
COPY target/*.jar address-svc.jar

ENTRYPOINT ["java","-jar","/address-svc.jar"]